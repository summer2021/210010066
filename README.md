error-injection

[TOC]

# Design Ideas(设计思想)
故障注入的核心在于，如何在复杂系统中，在小开销的情况下，尝试发现系统中细粒度的bug。而这些bug可能也许就是一个简单的整数溢出，又或者空指针解引用，但是其对系统本身的危害是极大的，轻则造成系统crash，重则被不法分子非法利用，进而造成重大损失。

我们首先分开调研了现有的比较成熟的解决方案（内核态/用户态）。

**内核态现有方案**

目前为止，世界上最好用的内核态漏洞挖掘的工具是基于系统调用的 [syzkaller](https://github.com/google/syzkaller) ，但是，syzkaller并不适合我们故障注入的场景。原因如下：
1. syzkaller是通过系统调用level的变异与序列化组合，来尝试发觉内核态的bug，但是这种方式粒度很粗，每一次syscall的变异，都牵扯到后续多个调用序列的传导变异。所以往往很难对于单个函数粒度有较好的覆盖率。
2. syzkaller每次启动都会启动多个OS kernel instance，导致了其上下文环境非常不稳定（dirty instance），bug的触发极度依赖上下文环境，有很多bug难以复现（reproduce）。
3. syzkaller不是基于原生的Linux内核Hook，所以难以做到精准的故障注入的控制。

基于以上的情况，我们提出了自己的想法。

**内核态故障注入实现思想**

我们希望借用Linux Kernel原生的hook方案，来实现更加高效、细粒度的故障注入。于是我们使用了 [Kprobe](https://www.kernel.org/doc/Documentation/kprobes.txt) 以及其衍生的 Kretprobe，他是内核态原生的一般用于观测函数行为的hook模块。我们通过对于内核源码进行简易的修改，使得 Kprobe 的探测点可以动态的添加。

当我们对一个内核函数添加一个 kprobe Hook点之后，内核执行到这个函数，会触发特殊的int3断点的处理例程，这个例程是基于 Kporbe handler 的。当我们定义好了对应的handler时，内核在 探测点 位置，会将控制流转向我们自定的 handler ， 而在handler中，我们可以实现对于 被探测函数 当前上下文情况，返回值等的修改与变异。

![](./doc/kprobe.png)

***

**用户态现有方案**

目前用户态比较主流的工具是American-Fuzz-Loop，简称 [AFL](https://github.com/google/AFL) ，但是仍有以下缺点：

1. AFL难以做到函数粒度的故障控制，而是通过直接对原始输入来变异进行故障注入。
2. AFL在针对闭源程序时只能通过QEMU用户模式在指令翻译时进行插桩，导致效率很低。

**用户态故障注入实现方案**

我们受到GDB调试器的启发，首先阅读了GDB调试器的源码，决定使用 ```ptrace``` 来实现用户态的功能。

基于此，**我们首先基于 ```ptrace``` 实现了一个简易的调试器（tracer.c）**，其基本功能是函数粒度的断点注入，维护断点列表，断点恢复等，确保上下文稳定。在此基础上，我们实现了简单可扩展的断点 handler，每当子进程HIT到一个断点，我们都可以将控制流转向handler，在handler中，可以对于用户态函数的返回值，参数等信息进行自定义的修改。

这样做的好处在于：

1. 快速高效，```ptrace``` 是一套很成熟的跨进程注入的模块，Unix兼容性很强，其速度也非常快。
2. 针对闭源程序，能原生的实现断点注入/插桩，进而在handler里完成故障的注入。

父进程（tracer）通过exec族函数在子进程中启动目标程序（tracee），先进行断点注入，然后进行监控。

由于父子进程的亲缘关系，他们之间可以方便的使用信号进行通信，父进程可以时时监控子进程的状态，并基于handler来实现断点的注入。

![](./doc/gdb.png)

***

**系统架构**

设计并初步完善了具有可扩展性的项目架构，我将他叫做 **<u>Injection-forserver</u>** 架构。

本架构的设计初衷是增强项目的可扩展性，具体来说设计了一个Server用来统筹控制相应功能模块的行为，而具体功能模块的运行由Server fork()出的子进程调用exec族函数启动。而我们的主Server通过命名管道（fifo_pipe）与对应的kprobe注册模块（./Register）通信，发送对应的控制命令等。而./Register通过ioctl与内核模块通信，并返回状态给Server，最后统一由server来告知用户对应信息。

具体架构图如下：
![](./doc/tools-arch.png)

# Environment Set-Up(环境搭建)
## Back-up Environment instance(备用环境实例)
- 请注意，如果您使用下面的step-by-step仍然因为一些原因搭建环境失败（请提交issue），我已经准备好了一个环境的实例，您可以登陆实例并体验本工具：
```sh
# ip:passwd
ssh root@52.188.124.138
root@52.188.124.138's password: Qwe.1234

# After you login
root@ubuntu:~# ls
210010066  linux-4.15

# Then, run the project by
cd 210010066/
./boot.sh 
```
***Util you successfully run the project, the test steps are the same as the following part: Examples***


## Step-by-step
- Clone the **newest** code.
  ```sh
  git clone --depth 1 https://gitlab.summer-ospp.ac.cn/summer2021/210010066.git
  ```
- Get compatible Linux Kernel Source Code.

  ```sh
  sudo wget https://mirror.tuna.tsinghua.edu.cn/kernel/v4.x/linux-4.15.tar.xz
  ```
- Unpack the package and apply the patch.

  ```sh
  # Prerequsites
  gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04)
  # Decompressed
  xz -d linux-4.15.tar.xz
  tar -xvf linux-4.15.tar
  cd linux-4.15
  git init
  mkdir patch
  cd ./patch
  cp ../../210010066/patch/* ./

  # Apply the .patch file in patch/
  rm ../include/linux/kprobes.h
  git am --signoff < ./0001-kprobes.h.patch
  rm ../include/linux/kallsyms.h
  git am --signoff < ./0001-kallsyms.h.patch
  rm ../kernel/kprobes.c
  git am --signoff < ./0002-kprobes.c.patch
  rm ../kernel/kallsyms.c
  git am --signoff ./0003-.patch
  rm ../kernel/module.c
  git am --signoff < ./0004-modules.c.patch
  rm ../include/linux/module.h
  git am --signoff < ./0005-modules.h.patch
  ```
- Compile the Kernel.

  ```sh
  apt install -y ncurses-dev libelf-dev libssl-dev bc
  make menuconfig
  # After menu poped, you just need to save the default config is ok.
  make bzImage -j8 
  ```
- Following from that, create kernel's new symbolic links.

  ```sh
  cd ~/210010066

  # Create  new symbolic links.
  ln -s ~/linux-4.15/vmlinux ~/210010066/vmlinux

  ln -s ~/linux-4.15/arch/x86/boot/bzImage ~/210010066/bzImage
  ```
- Finally, install QEMU

  ```sh
  apt update
  apt-get install -y qemu-kvm qemu virt-manager virt-viewer libvirt-bin bridge-utils
  ```
- (optional) install ```pwndbg```
  https://www.dazhuanlan.com/maxinping0819/topics/1138414
  https://www.jianshu.com/p/62db4d1275ba
 
# Files

- **```server```**
  > A controller for overall scheduling.

- **```boot.sh```**
  > A script boots the QEMU for Kernel.

- **```core/```**
  > A basic file-system, so you don't need to build from busybox by yourself , just ```cd core/``` and modify the ```core/```, and then ```sh gen.sh``` which will help you generate a new .img file for QEMU.

- **```kernel/kprobe_example.ko```**
  > An test example for kernel error injection.
  > Work with ```kernel/kernel_test.c```

- **```kernel/kernel_test.ko```**
  > An test example for kernel error injection**(User should run it to check the kernel error injection's result)**.

- **```kernel/check.ko```**
  > A LKM, which help us load/unload our customized probe.


- **```user/register```**
  > A Register for load/unload the probe.

- **```user/test```**
  > Syscall test examples.

- **```tracer```**
  > User-space hook point injection program.

- **```tracee```**
  > User-space hook point injection test example.

# Usage
- Firstly, make sure you have QEMU env.

- Secondly, modify the ```kerenl/Makefile```, and re-compile the LKM.
    ```Makefile
    ifneq  ($(KERNELRELEASE),)
    obj-m:=kprobe_example.o
    obj-m+=check.o
    else
    KDIR := /root/linux-4.15/ #/lib/modules/$(shell uname -r)/build
    PWD:=$(shell pwd)
    all:
            make -C $(KDIR) M=$(PWD) modules
            rm -f *.o *.symvers *.cmd *.cmd.o *.mod.c *.mod.o.cmd *.ko.cmd
            cp ./*.ko ../core/
    clean:
            rm -f *.ko *.o *.symvers *.cmd *.cmd.o
    endif
    ```
    ```$KDIR``` should be your Linux Source Code Dir.

- Thirdly, modify the ```./boot.sh``` 
  ```sh
  qemu-system-x86_64 -m 120M \
  -kernel {your_bzImage}  \
  -initrd {your_rootfs.img} \
  -append "console=ttyS0 root=/dev/ram rdinit=/sbin/init quiet nokaslr" -cpu qemu64 \
  -nographic \
  -gdb tcp::1234
  ```


- Finally, you can exec ```boot.sh``` to enter the System based on QEMU.  **But exec ```a.sh``` is more efficiently in order to build the whole system.**

# Examples
## Change KERNEL-SPACE the parameter registers of a function to injection the fault
- Our target function is ```test_test() in  kernel/kprobe_example.c``` which has a simple logic.
  ```c
      printk(KERN_ALERT "test_test(), original rdi:0, NOW rdi: %d\n",rdi);
    ull tmp;
    if(rdi == 0){
        tmp = (rdi*rsi)+rdx;
        printk(KERN_ALERT "rdi=0\ttmp: %d\n",tmp);
    }else if(rdi == 1){
        tmp = (rdi+rsi)+rdx;
        printk(KERN_ALERT "rdi=1\ttmp: %d\n",tmp);
    }else if(rdi == 2){
        tmp = (rdi-rsi)+rdx;
        printk(KERN_ALERT "rdi=2\ttmp: %d\n",tmp);
    }else{
        // invalid address execute
        __asm__ volatile(
        "mov err_addr, %r12;"
        "jmp %r12;"
        );
    }
    printk(KERN_ALERT "test_test(), ORIGINAL return value: %d\n",tmp);
    return tmp;
  ```
- Exec ```./server``` first, 
  ```sh
  / # ./server 
  [+] open pipe success: 3

  ========== Welecome to Injection Server ==========

  [1] Register a Kernel Hook Point
  [2] Unregister a Kernel Hook Point
  [3] Exit the Program

  ==================================================
  > 1
  # set our hook point
  [+] The Kernel Symbol Name or Address:
  test_test         
  [+] Please input the hook point:
  test_test
  # choose to change params
  [+] If change the arg? [0/1]
  1
  # set the params registers
  [+] Please input the arg1, arg2, arg3:
  1 1 1
  # this part, we don't need to change the return value.
  [+] If change the return value? [0/1]
  0
  in.if_hook_point_str:1
  [   40.622099] args->rdi:0x1, args->rsi: 0x1, args->rdx:0x1
  [*] Kprobe Add Success

  ========== Welecome to Injection Server ==========

  [1] Register a Kernel Hook Point
  [2] Unregister a Kernel Hook Point
  [3] Exit the Program

  ==================================================
  # crtl+Z, suspend the server to backend
  > ^Z[1]+  Stopped                    ./server
  ```
- And then we run ```./kerenl_test``` to check the result:

  ```sh
  / # ./kernel_test 
  [  101.224259] Change the params, default first 3 params
  [  101.225027] test_test(), original rdi:0, NOW rdi: 1
  [  101.225231] rdi=1    tmp: 3
  [  101.225562] test_test(), ORIGINAL return value: 3
  [  101.226310] test_test(), NOW return value: 0x3
  / # 
  ```
  It is easy to check that, after our injection, the rdi value has been changed from 0 -> 1. And it will cause the different logic in the following part, which leads to different tmp value.

## Change KERNEL-SPACE the return-value registers of a function to injection the fault
- First, we run ```server```
  ```sh
  / # ./server 
  [+] open pipe success: 3

  ========== Welecome to Injection Server ==========

  [1] Register a Kernel Hook Point
  [2] Unregister a Kernel Hook Point
  [3] Exit the Program

  ==================================================
  > 1
  [+] The Kernel Symbol Name or Address:
  test_test
  [+] Please input the hook point:
  test_test
  # this part, we don't need to change the params registers.
  [+] If change the arg? [0/1]
  0
  [+] If change the return value? [0/1]
  # we choose to modify the return value register
  1
  # input the return value we prefer: 0xffffffffffffffff
  [+] Please input the rax(HEX):
  0xffffffffffffffff
  in.if_hook_point_str:1
  [   47.770234] args->rdi:0x0, args->rsi: 0x0, args->rdx:0x0
  [*] Kprobe Add Success

  ========== Welecome to Injection Server ==========

  [1] Register a Kernel Hook Point
  [2] Unregister a Kernel Hook Point
  [3] Exit the Program

  ==================================================
  > ^Z[1]+  Stopped                    ./server
  ```
- After that, we can run ```./kernel_test``` to check the result.
  ```
  / # ./kernel_test 
  [  124.153807] test_test(), original rdi:0, NOW rdi: 0
  [  124.154073] rdi=0    tmp: 20
  [  124.154313] test_test(), ORIGINAL return value: 20
  [  124.155127] change rax to: 0xffffffffffffffff
  [  124.155505] test_test(), NOW return value: 0xffffffffffffffff
  / # 
  ```
  It is easy to check that, the original return value is 20, but after our injection, the return value has been changed to 0xffffffffffffffff.
## Change the USER-SPACE parameter registers and return value of a function to injection the fault

***Becasue it is the user-space program, you don't need to use QEMU***



> We decide to design and implement a injector in user space through the native ```ptrace()```
- First, you can easily compile the ```tracer``` and the example ```tracee``` by: 
  ```sh
  gcc tracer.c -o tracer
  gcc tracee.c -no-pie -o tracee  # make true tracee is no-pie
  ```
- After that, you will got ELF files: ```tracer``` and ```tracee```
- ```tracee``` has very simple logic
  ```c
      int tmp;
    if(rdi == 0){
        tmp = (rdi*rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else if(rdi == 1){
        tmp = (rdi+rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else if(rdi == 2){
        tmp = (rdi-rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else{
        // NULL pointer dereference exception.
        __asm__ __volatile__ (
            ".intel_syntax noprefix;"
            "mov rax, 0;"
            "mov rdx, [rax];"
            ".att_syntax;"
        );
    }
  ```
- Then, run the CMD: 
  ```
  ./tracer ./tracee
  ```
  Choose the function you want to hook:
  ```
  Function name?
  func3
  [+] Tracing PID:5845
  ```
  When tracer hit a breakpoint, choose your mode:
  ```
  【ret value】 0x1
  HIT func3()
  1. Change the Params
  2. Change the return value
  3. Skip
  ```
  Let's Change the rdi:
  ```
  HIT func3()
  1. Change the Params
  2. Change the return value
  3. Skip
  1
  1. Change rdi
  2. Change rsi
  3. Chaneg rdx
  1
  New RDI:
  111111111111
  Perhaps catch an exception:
  signal: 0x7f
  ip: 0x4005a0
  ```
  It is easy to confirm that, our tracer successfully cactch an EXCEPTION occured in child process in $RIP:0x4005a0.

  The next time, let's Change the return value by modify RAX register:
  ```sh
  Function name?
  func2
  [+] Tracing PID:6020


  [+] Start
  HIT func2()
  1. Change the Params
  2. Change the return value
  3. Skip
  2
  ret addr: 0x4005be; the ret bp has been inject

  The ret vaule will be?
  0x1000
  【ret value】 0x1000
  tmp: 20

  [+] Child process EXITED!
  ```
- That's ALL : )
