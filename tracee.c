// gcc tracee.c -no-pie -o tracee
#include <stdio.h>
#include <stdlib.h>
size_t my_rdi=0;
size_t my_rsi=0;
size_t my_rdx=0;

// This function we use to test return value.
int func1()
{
    return 0;
}

int func2()
{
   
    return 1;
}

// This functions we use to test param
void func3(int rdi,int rsi,int rdx)
{
    int tmp;
    if(rdi == 0){
        tmp = (rdi*rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else if(rdi == 1){
        tmp = (rdi+rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else if(rdi == 2){
        tmp = (rdi-rsi)+rdx;
        printf("tmp: %d\n",tmp);
    }else{
        // NULL pointer dereference exception.
        __asm__ __volatile__ (
            ".intel_syntax noprefix;"
            "mov rax, 0;"
            "mov rdx, [rax];"
            ".att_syntax;"
        );
    }

    return ;
}

int main()
{
    //printf("===========\n");
    func1();
    printf("【ret value】 %#lx\n",func2());
    func3(0,10,20);
    //printf("===========\n");
    return 0;
}    
