#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdint.h>
#include<string.h>
#define N 0x100

const char *fifo_name = "register_pipe";
uint8_t buffer[N] = {0};
int pipe_fd;
const int open_mode = O_RDWR; 
char read_buf[N]={0};
char write_buf[N]={0};

/*  check if expr==-1 */
#define CHECK(expr)     \
    if((expr) < 0){    \
        do{             \
            perror(#expr);  \
            exit(EXIT_FAILURE); \
        } while (0);    \
    }                   
/*  check if expr==-1 */

int add_kernel_hook_point();
int unload_kernel_hook_point();


void menu(){
    puts("\n========== Welecome to Injection Server ==========\n");
    puts("[1] Register a Kernel Hook Point");
    puts("[2] Unregister a Kernel Hook Point");
    puts("[3] Exit the Program\n");
    puts("==================================================");
    printf("> ");
}

__always_inline int write_to_pipe(void *addr,int size){
    /* Wrapper: Write to Pipe and Check */
    int ret=0;
    ret = write(pipe_fd,addr,size);
    if(ret == size){
        ret = 1;
    }
    else{
        ret = -1;
    }
    return ret;
}

__always_inline int read_from_pipe(void *addr,int size){
    /* Wrapper: Read from Pipe and Check */
    int ret=0;
    ret = read(pipe_fd,addr,size);
    ret = 1;
    return ret;
}

int add_kernel_hook_point(){
    
    int len;
    int ret;
    printf("[+] The Kernel Symbol Name or Address:\n");
    scanf("%20s",write_buf);
    len = strlen(write_buf);
    CHECK(write_to_pipe(write_buf,len));
    //printf("[+] Write Success\n");
    sleep(1);
    CHECK(read_from_pipe(read_buf,sizeof(read_buf)));
    //printf("[+] Server read : %s \n",read_buf);
    if(!strcmp(read_buf , "success")){
        printf("[*] Kprobe Add Success\n");
        ret = 1;
    } 
    else if(!strcmp(read_buf, "fail")){
        printf("[-] Kprobe Add Failed\n");
        ret = -1;
    }
    return ret;
}


int unload_kernel_hook_point(){
    return 0;
}

int main(int argc, char * argv[])
{
    int res;
    int ret;
    int choice=0;
    if(access(fifo_name, F_OK) == -1)  
    {  
        res = mkfifo(fifo_name, 0777);  
        if(res != 0)  
        {  
            fprintf(stderr, "Could not create fifo %s\n", fifo_name);  
            exit(EXIT_FAILURE);  
        }  
    }  

    /* Open the pipe */
    pipe_fd = open(fifo_name, open_mode);  
    CHECK(pipe_fd);
    printf("[+] open pipe success: %d\n",pipe_fd);
    
    /* Start the Register */
    // system("./register &");
    int pid = fork();
    if(pid==0){
        char *envp[]={0,NULL}; 
        char *argv[]={"&"};
        execve("./register",argv,envp);
    }

    while(1){

        menu();
        if(scanf("%d",&choice) != 1){
            puts("[-] Scanf Failed");exit(-1);
        }
        //printf("choice is : %d\n",choice);
        switch (choice)
        {
        case 1:
            ret = add_kernel_hook_point();
            break;
        case 2:
            ret = unload_kernel_hook_point();
            break;
        case 3:
            printf("Bye~\n");
            exit(0);
            break;


        default:
            printf("[-] Unexpected Choice : %d\n",choice);
            exit(-1);
        }
    }

    return 0;
}