#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/bpf.h>
#include <unistd.h>
#include <sys/syscall.h>
char a[] = "this is a example\n";
long long len=0;
unsigned long long  addr = a;
union bpf_attr *attr;


__attribute__((constructor)) static void Init ( void )
{
    len = strlen(a);
    attr = malloc(0x100);
}

//0xffffffff8125cb90 T sys_write
void SYS_Write(){
    __asm__ volatile(
        "xor rax,rax;"
        "mov rax, 1;"
        "mov rdi, 1;"
        "mov rsi, addr;"
        "mov rdx, len;"
        "syscall"
    );
    printf("%p\n",a);
}

// 0xffffffff8118e040 T sys_bpf
void SYS_Bpf(){
    attr->map_type = 0x16;    
    attr->max_entries = -1;
    attr->map_flags = 0;
    attr->value_size = 0x40;
    //attr->key = 0;
    syscall(__NR_bpf,BPF_MAP_CREATE,attr,0x30);
}
/*
pwndbg> bt
#0  SyS_bpf (cmd=0, uattr=15579936, size=48) at kernel/bpf/syscall.c:1685
#1  0xffffffff81a00091 in entry_SYSCALL_64 () at arch/x86/entry/entry_64.S:276
#2  0x0000000000000000 in ?? ()
pwndbg> x /10gx 0xffffffff81a00091
0xffffffff81a00091 <entry_SYSCALL_64+113>:      0x6666fa5024448948      0x251c8b4c65906690
*/

int main(){
    SYS_Bpf();
    return 0;
}