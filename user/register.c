#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdint.h>
#include<string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>

const char *fifo_name = "register_pipe";
char flush[0x100];
typedef unsigned long long ull;

/*  check if expr==-1 */
#define CHECK(expr)     \
    if((expr) < 0){    \
        do{             \
            perror(#expr);  \
            exit(EXIT_FAILURE); \
        } while (0);    \
    }                   
/*  check if expr==-1 */

struct args{
    void *addr;
    uint64_t len;
    unsigned long long pre_jmp_addr;
    ull if_hook_point_str;
    unsigned long long post_jmp_addr;
    int choice;
    ull reg_flag;
    ull rdi,rsi,rdx,rcx,rax,rsp,rbp;
};

void pre_hook(){
    printf("pre_hook\n");
}

char str[0x20];
uint64_t fd,fd2;
char read_buf[0x100]={0};
char write_buf[0x100]={0};
const char *success = "success";
const char *fail = "fail";
char tmp[0x100]={0};


void Read(int fd, char *addr,size_t n){
    char chr;
    while(1){
        read(fd,&chr,0x1);
        if(chr == '\x0a'){
            return;
        }
        else{
            --n;
            if(n==0){
                return;
            }
            *(addr++) = chr;
        }
    }
}

int add_kprobe(char addr[],uint64_t len){
    int flag=0;
    int ret_flag=0;
    ull rdi=0;
    ull rsi=0;
    ull rdx=0;
    ull rax=0;
    struct args in;

    puts("[+] If change the arg? [0/1]");
    scanf("%d",&flag);
    if(flag==1){
        puts("[+] Please input the arg1, arg2, arg3:");
        scanf("%llx%llx%llx",&rdi,&rsi,&rdx);
    }

    puts("[+] If change the return value? [0/1]");
    scanf("%d",&ret_flag);
    if(ret_flag==1){
        puts("[+] Please input the rax(HEX):");
        scanf("%llx",&rax);
    }
    


    if(strstr(addr,"0xf")){
        in.if_hook_point_str = 0;
    }else{
        in.if_hook_point_str = 1;
    }
    
    printf("in.if_hook_point_str:%d\n",in.if_hook_point_str);

    in.addr = str;
    in.len = len;
    in.pre_jmp_addr = addr;
    in.post_jmp_addr = 0x0;
    in.rdi = rdi;
    in.rsi = rsi;
    in.rdx = rdx;
    in.rax = rax;
    in.choice = 0;
    return ioctl(fd,0,&in);
}

int main(int argc,char *argv[]){

    memset(flush,0xcc,0x100);
    //printf("[+] Pre_hook addr:%p\n",pre_hook);
    long long ret=0;
    fd = open("/proc/check",O_RDWR);
    CHECK(fd);

    int pipe_read_fd = open(fifo_name,O_RDONLY);
    int pipe_write_fd = open(fifo_name,O_WRONLY);
    CHECK(pipe_read_fd);
    CHECK(pipe_write_fd);

while(1){

    /* Read Control CMD from server */
    
    read(pipe_read_fd,read_buf,sizeof(read_buf));
    //printf("\"Register\" Successful read from pipe: %s\n",read_buf);
    
      

    memcpy(str,read_buf,sizeof(read_buf));
    uint64_t len = strlen(str);
    
    puts("[+] Please input the hook point:");
    Read(0,tmp,0x100);
    //fgets(tmp,0x100,stdin);
    //scanf("%llx",&tmp);
    

    if(strcmp(tmp,"") != 0){
        ret = add_kprobe(tmp,len);
    }else{
        ret = add_kprobe(pre_hook,len);
    }



    //printf("Kprobe: %s\n",in.addr);
    //kprobe add
    

    

    /* Write Back the Status */
    if(ret == 1){
        write(pipe_write_fd,success,sizeof(success));
    }
    else if(ret < 0){
        write(pipe_write_fd,fail,sizeof(fail));
        exit(-1);
    }

    /* fflush the pipe buffer */
    sleep(1);
    memset(read_buf,0,sizeof(read_buf));  
    memset(write_buf,0,sizeof(write_buf));
    ret = 0;

}
   //close(fd);
    return 0;


}

