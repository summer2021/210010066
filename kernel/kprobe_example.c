#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/kprobes.h>
#include <linux/kernel.h>
#include <linux/hardirq.h>
#include <linux/sched.h>
#include <linux/preempt.h>
#include <linux/ftrace.h>
#include <linux/fs.h>           // for basic filesystem
#include <linux/proc_fs.h>      // for the proc filesystem
#include <linux/seq_file.h>     // for sequence files

typedef unsigned long size_t;
typedef unsigned long long ull;
static struct proc_dir_entry* test_file;
unsigned long long  pre_jmp_addr;
unsigned long long  post_jmp_addr;
int   err_addr=0;


size_t ret_addr=0;

int count =0;
struct in_args{
    void *addr;
    int len;  
    unsigned long long pre_jmp_addr;
    ull if_hook_point_str;
    unsigned long long post_jmp_addr;
    int choice;
    ull reg_flag;
    ull rdi,rsi,rdx,rcx,rax,rsp,rbp;
};

static int test_test(int rdi,int rsi,int rdx) noinline ;

static int 
test_test(int rdi,int rsi,int rdx){
    printk(KERN_ALERT "test_test(), original rdi:0, NOW rdi: %d\n",rdi);
    ull tmp;
    if(rdi == 0){
        tmp = (rdi*rsi)+rdx;
        printk(KERN_ALERT "rdi=0\ttmp: %d\n",tmp);
    }else if(rdi == 1){
        tmp = (rdi+rsi)+rdx;
        printk(KERN_ALERT "rdi=1\ttmp: %d\n",tmp);
    }else if(rdi == 2){
        tmp = (rdi-rsi)+rdx;
        printk(KERN_ALERT "rdi=2\ttmp: %d\n",tmp);
    }else{
        // invalid address execute
        __asm__ volatile(
        "mov err_addr, %r12;"
        "jmp %r12;"
        );
    }
    printk(KERN_ALERT "test_test(), ORIGINAL return value: %d\n",tmp);
    return tmp;
}
//EXPORT_SYMBOL_GPL(test_test);



static ull 
_abs(ull a,ull b){
    if(a>b)
        return a-b;
    else if(a==b)
        return 0;
    else
        return b-a;
}


static long test_ioctl(struct file *file, unsigned int cmd, size_t arg)
{
    
    long long ret = 0;
    unsigned long long  addr;
    struct in_args in;
    if(copy_from_user(&in, (void *)arg, sizeof(in))){
	    return ret;
    }
    cmd = in.choice;
    ret = test_test(in.rdi,in.rsi,in.rdx);
    printk(KERN_ALERT "test_test(), NOW return value: %#lx\n",ret);
   
    return ret;
}

static int
test_show(struct seq_file *m, void *v)
{
    return 0;
}
static int
test_open(struct inode *inode, struct file *file)
{
     return single_open(file, test_show, NULL);
}

static const struct file_operations test_fops = {
    .owner    = THIS_MODULE,
    .open    = test_open,
    .read    = seq_read,
    .llseek    = seq_lseek,
    .release    = single_release,
    .unlocked_ioctl = test_ioctl,
};




static int __init
test_init(void)
{
    test_file = proc_create("test", 0, NULL, &test_fops);
 
    if (!test_file) {
        return -ENOMEM;
    }

    return 0;
}
 
static void __exit
test_exit(void)
{
    remove_proc_entry("test", NULL);
}
 
module_init(test_init);
module_exit(test_exit);
 
MODULE_LICENSE("GPL");