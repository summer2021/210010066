// gcc kernel_test.c -static -o kernel_test
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdint.h>
#include<string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
typedef unsigned long long ull;
struct in_args{
    void *addr;
    int len;  
    unsigned long long pre_jmp_addr;
    ull if_hook_point_str;
    unsigned long long post_jmp_addr;
    int choice;
    ull reg_flag;
    ull rdi,rsi,rdx,rcx,rax,rsp,rbp;
};

int main(){
    int fd = open("/proc/test",O_RDWR);
    if(fd < 0){
        perror("fd:");
    }
    struct in_args arg;
    arg.rdi = 0;
    arg.rsi = 10;
    arg.rdx = 20;
    arg.choice = 0xdeadbeef;
    ioctl(fd,0,&arg);
}