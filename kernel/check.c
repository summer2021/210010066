#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/kprobes.h>
#include <linux/kernel.h>
#include <linux/hardirq.h>
#include <linux/sched.h>
#include <linux/preempt.h>
#include <linux/ftrace.h>
#include <linux/fs.h>           // for basic filesystem
#include <linux/proc_fs.h>      // for the proc filesystem
#include <linux/seq_file.h>     // for sequence files

typedef unsigned long size_t;
typedef unsigned long long ull;
static struct proc_dir_entry* check_file;
unsigned long long  pre_jmp_addr;
unsigned long long  post_jmp_addr;
void *  err_addr;

size_t ori_rbp=0;

size_t ori_rsp=0;
size_t ori_rcx=0;
size_t ori_rdx=0;
size_t ori_rsi=0;
size_t ori_rdi=0;
size_t ori_rip=0;
size_t ori_rbx=0;
size_t ori_rax=0;
size_t ori_r8=0;
size_t ori_r9=0;

size_t chg_rbp=0;
size_t chg_rsp=0;
size_t chg_rcx=0;
size_t chg_rdx=0;
size_t chg_rsi=0;
size_t chg_rdi=0;
size_t chg_rip=0;
size_t chg_rax=0;

size_t if_change_ret_value=0;
size_t ret_addr=0;

int count =0;
struct in_args{
    void *addr;
    int len;  
    unsigned long long pre_jmp_addr;
    ull if_hook_point_str;
    unsigned long long post_jmp_addr;
    int choice;
    ull reg_flag;
    ull rdi,rsi,rdx,rcx,rax,rsp,rbp;
};
static void  kprobe_unload(void);
static size_t kretprobe_add(struct in_args *args);

static int check_test(int rdi,int rsi,int rdx) noinline ;


static int
check_show(struct seq_file *m, void *v)
{
    return 0;
}

static int
check_open(struct inode *inode, struct file *file)
{
     return single_open(file, check_show, NULL);
}

static int 
check_write(struct in_args *args)
{
    return 0;
}
// /__attribute__((noinline)) 
static int 
check_test(int rdi,int rsi,int rdx){
    printk(KERN_ALERT "check_test()\n");
    ull tmp;
    if(rdi == 0){
        tmp = (rdi*rsi)+rdx;
        printk(KERN_ALERT "rdi=0\ttmp: %d\n",tmp);
    }else if(rdi == 1){
        tmp = (rdi+rsi)+rdx;
        printk(KERN_ALERT "rdi=1\ttmp: %d\n",tmp);
    }else if(rdi == 2){
        tmp = (rdi-rsi)+rdx;
        printk(KERN_ALERT "rdi=2\ttmp: %d\n",tmp);
    }
    return tmp;
}
//EXPORT_SYMBOL_GPL(check_test);

static void 
pre_hook(void){
    printk("Hook success");
    return;
}


static ull 
_abs(ull a,ull b){
    if(a>b)
        return a-b;
    else if(a==b)
        return 0;
    else
        return b-a;
}

static unsigned long long  revise_jmp(unsigned long long pre_jump,unsigned long long ori_rip){
    if(pre_jump==0){
        goto out;
    }else{
        if( _abs(pre_jump,ori_rip) < 3){
            pre_jump = ori_rip;
            kprobe_unload();
            goto out;
        }
        else{
            goto out;
        }
    }
out:
    //printk("[+] After revise: %#llx",pre_jump);
    return pre_jump;
}
static int handler_pre_ret(struct kprobe *p, struct pt_regs *regs){

}
static struct kprobe kp_ret;
static int handler_pre(struct kprobe *p, struct pt_regs *regs)
{
    // //regs->ip = 0xffffffffffffffff;
    // printk(KERN_ALERT "0xffffffffffffffff");
    // //return 1;
    // /* $pc  */
    // ori_rip = regs->ip;
    // /* Return Value */
    // ori_rax = regs->ax;
    // /* Arguments Register List */
    // ori_rdi = regs->di;
    // ori_rsi = regs->si;
    // ori_rdx = regs->dx;
    // ori_rcx = regs->cx;
    // ori_rsp = regs->sp;
    // ori_rbp = regs->bp;
    // ori_rbx = regs->bx;
    // ori_r8 = regs->r8;
    // ori_r9 = regs->r9;

    
    // revise_jmp 修正跳转地址，如果ori_rip和pre_jmp非常近的话，判定为我们想要修改参数值，进而需要重新进入函数
    pre_jmp_addr = revise_jmp(pre_jmp_addr,ori_rip);
    //printk(KERN_ALERT "pre_jmp_addr:%#lx",pre_jmp_addr);
    //  当需要对参数进行更改时
    if(chg_rdi!=0 || chg_rsi!=0 || chg_rdx!=0)
    {
        printk(KERN_ALERT "Change the params, default first 3 params\n");
        regs->di = chg_rdi;
        regs->si = chg_rsi;
        regs->dx = chg_rdx;
    }
    else
    {
        // __asm__ volatile(
        // "mov pre_jmp_addr, %r12;"
        // "jmp %r12;"
        // );
        printk(KERN_ALERT "Only Jump\n");
        regs->ip = pre_jmp_addr;
    }
    return 0;
}

static void 
handler_post(struct kprobe *p, struct pt_regs *regs,unsigned long flags)
{
    printk("Exit\trax: %#lx\trip: %#lx\n",regs->ax,regs->ip);

    /* $pc  */
    size_t ori_rip = regs->ip;

    /* Return Value */
    size_t ori_rax = regs->ax;

    /* Arguments Register List */
    size_t ori_rdi = regs->di;
    size_t ori_rsi = regs->si;
    size_t ori_rdx = regs->dx;
    size_t ori_rcx = regs->cx;
}

long 
check_start(struct in_args *args){
    /*  本函数中，先检测一下我们要hook的kernel symbol是否是合法的kernel symbol */
    ull  ret;
    char *name = args->addr;
    char *name_2 = args->pre_jmp_addr;
        // user give us a str 
    if(args->if_hook_point_str == 1){
        //printk(KERN_ALERT "in.pre_jmp_addr:%s",name_2);
        args->pre_jmp_addr = kallsyms_lookup_name(name_2);
        if(args->pre_jmp_addr == 0){
            printk(KERN_ALERT "[-] Cannot Get Pre_jump Kernel Symbol Addr\n");
            return 0;
        }
        //printk(KERN_ALERT "Successfully lookup prejump addr:%#llx",args->pre_jmp_addr);
    }   

    ret = kallsyms_lookup_name(name);
   //printk(KERN_ALERT "kallsyms_lookup_name(name):%#llx",ret);
    return ret;
}

static struct kprobe kp;

static size_t 
kprobe_add(struct in_args *args){
    long long ret = 0;
    ret = check_start(args);
    if(ret == 0){
        printk("[-] Cannot Get Kernel Symbol Addr\n");
        return -0x100;
    }
    printk("[*] Get Kernel Symbol address: %#lx\n",ret);

    pre_jmp_addr = args->pre_jmp_addr;
    post_jmp_addr = args->post_jmp_addr;
    kp.symbol_name = args->addr;
    
    printk(KERN_ALERT "args->rdi:%#lx, args->rsi: %#lx, args->rdx:%#lx\n",args->rdi,args->rsi,args->rdx);
    if(args->rdi!=0 || args->rsi!=0 || args->rdx!=0){
        if(args->rdi!=0)
            chg_rdi = args->rdi;
        if(args->rsi!=0)
            chg_rsi = args->rsi;
        if(args->rdx!=0)
            chg_rdx = args->rdx;
    }
    if(args->rax != 0){
        chg_rax = args->rax;
        kretprobe_add(args);
        return 1;
    }
    kp.pre_handler = handler_pre;
    //kp.post_handler = handler_post;
    ret = register_kprobe(&kp);
    printk("ret: %d",ret);
    if (ret < 0) {
            printk(KERN_INFO "register_kprobe failed, returned %d\n", ret);
            return ret;
    }
    printk(KERN_INFO "[+] Planted kprobe at %p\n", kp.addr);
    return 1;
}

static void 
kprobe_unload(void){
    unregister_kprobe(&kp);
    printk(KERN_INFO "kprobe at %p unregistered\n", kp.addr);
    return 0;
}

static int ret_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{   
    
    ori_rip = regs->ip;pre_jmp_addr = revise_jmp(pre_jmp_addr,ori_rip);
    ori_rax = regs->ax;
    ori_rdi = regs->di;
    ori_rsi = regs->si;
    ori_rdx = regs->dx;
    ori_rcx = regs->cx;
    ori_rsp = regs->sp;
    ori_rbp = regs->bp;
    ori_rbx = regs->bx;
    ori_r8 = regs->r8;
    ori_r9 = regs->r9;
    printk(KERN_ALERT "change rax to: %#lx\n",chg_rax);
    // __asm__ volatile (
    //     "mov chg_rax, %rax;"
    //     "mov ori_rdi, %rdi;"
    //     "mov ori_rsi, %rsi;"
    //     "mov ori_rdx, %rdx;"
    //     "mov ori_rbx, %rbx;"
    //     "mov ori_rcx, %rcx;"
    //     "mov ori_r8, %r8;"
    //     "mov ori_r9, %r9;"
    //     "mov ori_rbp, %rbp;"
    //     "mov ori_rsp, %rsp;"
    //     "mov pre_jmp_addr, %r12;"
    //     "jmp %r12;"
        
    // );
    regs->ax = chg_rax;
    return 0;
}

static struct kretprobe my_kretprobe = {
    .handler        = ret_handler,
    .maxactive        = 20,
};

static size_t
kretprobe_add(struct in_args *args){
    
    int ret;
    my_kretprobe.kp.symbol_name = args->addr;
    my_kretprobe.handler = ret_handler;
    ret = register_kretprobe(&my_kretprobe);
    if (ret < 0) {
            printk(KERN_INFO "register_kretprobe failed, returned %d\n", ret);
            return ret;
    }
    printk(KERN_INFO "[+] Planted kretprobe at %#lx\n", my_kretprobe.kp.addr);

}


static long check_ioctl(struct file *file, unsigned int cmd, size_t arg)
{
    
    long long ret = -EINVAL;
    unsigned long long  addr;
    struct in_args in;
    if(copy_from_user(&in, (void *)arg, sizeof(in))){
	    return ret;
    }




    cmd = in.choice;
    
    printk("[+] CMD: %#x\n",cmd);

    if(cmd == 0){
        ret = kprobe_add(&in);
    }
    else if(cmd == 1){
        ret = kretprobe_add(&in);
        if(ret<0){printk("[-] kretprobe_add Failed");}
    }else if(cmd == 0xdeadbeef){
        ret = check_test(in.rdi,in.rsi,in.rdx);
        printk("check_test return value: %#lx\n",ret);
    }
    return ret;
}



static const struct file_operations check_fops = {
    .owner    = THIS_MODULE,
    .open    = check_open,
    .read    = seq_read,
    .llseek    = seq_lseek,
    .release    = single_release,
    .unlocked_ioctl = check_ioctl,
};




static int __init
check_init(void)
{
    check_file = proc_create("check", 0, NULL, &check_fops);
 
    if (!check_file) {
        return -ENOMEM;
    }

    return 0;
}
 
static void __exit
check_exit(void)
{
    remove_proc_entry("check", NULL);
}
 
module_init(check_init);
module_exit(check_exit);
 
MODULE_LICENSE("GPL");